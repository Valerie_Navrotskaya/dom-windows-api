
function downloader(url) {
    return fetch(url)
    .then(res => res.json())
    .then(data => {
        return data;
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
}

function getCheckedValue() {
    const radioes = document.getElementsByName('r1');
    for (let i = 0; radioes.length - 1; i++) {
        if (radioes[i].checked) {
            return radioes[i].value;
        }
    }
}

function sort() {
    const radio = getCheckedValue();
    switch(radio) {
        case 'desDate':
            sortByDate(true);
            break;
        case 'ascDate':
            sortByDate(false);
            break;
         case 'desAps':
            sortByEpisodes(true);
            break;
        case 'ascsAps':
            sortByEpisodes(false);
            break;
    }
}

function sortByDate(descending) {
    const list = document.getElementById('container');
    let switching = true;
    let i;
    while (switching) {
        i = 0;
        switching = false;
        const element = list.getElementsByClassName('main');
            for (i = 0; i < element.length - 1; i++) {
            shouldSwitch = false;
            if (descending ?
                new Date(element[i].getElementsByClassName('created')[0].innerHTML) < new Date(element[i + 1].getElementsByClassName('created')[0].innerHTML) :
                new Date(element[i].getElementsByClassName('created')[0].innerHTML) > new Date(element[i + 1].getElementsByClassName('created')[0].innerHTML) 
                ) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            element[i].parentNode.insertBefore(element[i + 1], element[i]);
            switching = true;
        }
    }
}

function sortByEpisodes(descending) {
    const list = document.getElementById('container');
    let switching = true;
    let i;
    while (switching) {
        i = 0;
        switching = false;
        const element = list.getElementsByClassName('main');
            for (i = 0; i < element.length - 1; i++) {
            shouldSwitch = false;
            if (descending ?
                element[i].getElementsByClassName('episodes').length < element[i + 1].getElementsByClassName('episodes').length :
                element[i].getElementsByClassName('episodes').length > element[i + 1].getElementsByClassName('episodes').length
                ) {
                shouldSwitch = true;
                break;
            }
            if (
                element[i].getElementsByClassName('episodes').length === element[i + 1].getElementsByClassName('episodes').length &&
                descending ?
                new Date(element[i].getElementsByClassName('created')[0].innerHTML) < new Date(element[i + 1].getElementsByClassName('created')[0].innerHTML) :
                new Date(element[i].getElementsByClassName('created')[0].innerHTML) > new Date(element[i + 1].getElementsByClassName('created')[0].innerHTML)
            ) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            element[i].parentNode.insertBefore(element[i + 1], element[i]);
            switching = true;
        }
    }
}

async function htmlCreater(data) {
    for (const element of data) {
        const mainDiv = document.createElement("DIV");
        mainDiv.className = 'main';
        mainDiv.id = element.id

        const name = document.createElement("SPAN");
        name.className = 'name';
        name.appendChild(document.createTextNode(element.name));
        mainDiv.appendChild(name);

        const remove = document.createElement("SPAN");
        remove.className = 'remove';
        remove.appendChild(document.createTextNode('X'));
        remove.onclick = function() {
            document.getElementById(element.id).remove();
        }
        mainDiv.appendChild(remove);

        const image = document.createElement("IMG");
        image.className = 'image';
        image.src = element.image;
        mainDiv.appendChild(image);

        const species = document.createElement("SPAN");
        species.className = 'species';
        species.appendChild(document.createTextNode(element.species));
        mainDiv.appendChild(species);

        const location = document.createElement("DIV");
        location.className = 'location';
        location.appendChild(document.createTextNode(element.location.name));
        mainDiv.appendChild(location);

        const created = document.createElement("SPAN");
        created.className = 'created';
        created.appendChild(document.createTextNode(element.created));
        mainDiv.appendChild(created);

        const episodes = document.createElement("DIV");
        episodes.className = 'episodes';
        episodes.appendChild(document.createTextNode('Episodes:'));
        for (const url of element.episodes) {
            const episode = await downloader(url);
            const episodeName = document.createElement("P");
            episodeName.appendChild(document.createTextNode(episode.name));
            episodes.appendChild(episodeName);
        }
        mainDiv.appendChild(episodes);

        document.getElementById("container").appendChild(mainDiv);
    }
}

async function main() {
    const response = await downloader('https://rickandmortyapi.com/api/character');
    const data = response.results.map(character => {
        return {
            created: character.created,
            species: character.species,
            image: character.image,
            episodes: character.episode || [],
            name: character.name,
            location: character.location,
            id: character.id
        };
    });
    await htmlCreater(data);
    search.oninput = function() {
        for (const element of document.getElementsByClassName('main')) {
            element.style.display = (element.getElementsByClassName('name')[0].innerHTML.toLowerCase().includes(search.value.toLowerCase()) || !search.value) ? 'inline-block' : 'none';
        }
    };
    search.disabled = false;
}

main();